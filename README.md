# Codeclimate Shell

The point of this repository is to configure the codeclimate report to work without Docker runner. 

## Step 1: Configure the runner system

These steps are generic linux steps, though the `jq` installation is debian/ubuntu so replace that command with yum or rpm in redhat. 

a) Install code climate on a system. 

```bash
url -L https://github.com/codeclimate/codeclimate/archive/master.tar.gz | tar xvz
cd codeclimate-* && sudo make install
```

### Problem: This seems to require docker anyway

When looking at [the codeclimate makefile](https://github.com/codeclimate/codeclimate/blob/master/Makefile)
the entire thing is just docker pull and docker run. 

This is stated in the [prerequisites in the codeclimate readme](https://github.com/codeclimate/codeclimate#prerequisites). 

The fact hat I had docker installed on the system where I registered the shell executor may invalidate some use cases. 

b) Install `jq`

```bash
apt-get install jq
```


## Step 2: configure the project 

a) Registere the runner

gitlab-runner register ...

* Set the URL
* Set the token
* Give it a tag like "codeclimate"
* Make it a "shell" type

b) Configure the runner

* Go into gitlab project/group settings where the runner was registered
* Set the runner to protected if the project/group is public (this will prevent unauthorized users from making an MR and running arbitrary scripts)
* Set the job to pickup untagged jobs if you want it to run more than just code climate jobs

c) add job to CI definition

the job should look like this: 

```yaml
codeclimate-shell:
  script:
    - codeclimate engines:install
    - codeclimate analyze -f json > raw_codeclimate.json
    - jq -c 'map(select(.type | test("issue"; "i")))' raw_codeclimate.json > "gl-code-quality-report.json"
  artifacts: 
    reports:
      codequality: gl-code-quality-report.json
  tags: 
    - codeclimate
```

d) add the codeclimate plugin definition to the `.codeclimate.yml` file

```yaml
plugins:
  sonar-java:
    enabled: true
```

## Step 3: Run some CI pipelines

Now that the runner is setup, the project is configured, and a pipeline is defined, it should be ready to run. 